define('two/intriguer/ui', ['two/intriguer', 'two/locale', 'two/ui', 'two/ui/autoComplete', 'two/FrontButton', 'two/utils', 'two/eventQueue', 'ejs', 'struct/MapData', 'cdn'], function(IntrigueMaster, Locale, Interface, autoComplete, FrontButton, utils, eventQueue, ejs, $mapData, cdn) {
    var ui
    var opener
    var $window
    var bindEvents = function() {
    }
    var IntriguerInterface = function() {
        ui = new Interface('IntrigueMaster', {
            activeTab: 'intriguer',
            template: '__intriguer_html_window',
            css: '__intriguer_css_style',
            replaces: {
                locale: Locale,
                version: IntrigueMaster.version,
                types: ['village', 'character']
            }
        })
        opener = new FrontButton(Locale('intriguer', 'title'), {
            classHover: false,
            classBlur: false,
            onClick: function() {
                ui.openWindow()
            }
        })
        $window = $(ui.$window)
        bindEvents()
        IntrigueMaster.interfaceInitialized = true
        return ui
    }
    IntrigueMaster.interface = function() {
        IntrigueMaster.interface = IntriguerInterface()
    }
})