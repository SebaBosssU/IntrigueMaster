require([
    'two/ready',
    'two/intriguer',
    'two/intriguer/ui'
], function (
    ready,
    IntrigueMaster
) {
    if (IntrigueMaster.initialized) {
        return false
    }

    ready(function () {
        IntrigueMaster.init()
        IntrigueMaster.interface()
        IntrigueMaster.run()
    })
})
