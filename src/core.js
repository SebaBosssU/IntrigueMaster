define('two/intriguer', [
    'two/locale',
    'two/ready'
], function (
    Locale,
    ready
) {
    var IntrigueMaster = {}
    IntrigueMaster.version = '__intriguer_version'
    IntrigueMaster.init = function () {
        Locale.create('intriguer', __intriguer_locale, 'pl')

        IntrigueMaster.initialized = true
    }
    IntrigueMaster.run = function () {
        if (!IntrigueMaster.interfaceInitialized) {
            throw new Error('IntrigueMaster interface not initialized')
        }
        ready(function () {
            $player = modelDataService.getSelectedCharacter()
        }, ['initial_village'])
    }
    return IntrigueMaster
})